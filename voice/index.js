const exec = require('child_process').exec;
const path = require('path');
const dir = __dirname;

/**
 * Преобразование текст в wav-файл
 * @param text - текст для преобразования
 * @param cb - результаты f(err,filepath)
 */
module.exports = (text,cb)=>{
    let filename = `${Date.now()}.wav`;
    let filepath = path.join(dir, '..','tmp', filename);
    text=text.replace(/[^а-я- \d]/gmi,'');
    let cmd=`echo "${text}" | /usr/bin/text2wave -otype wav -eval '(voice_msu_ru_nsh_clunits)' -o ${filepath}`;
    exec(cmd).on('close', (code) => {
        console.log(`[VOICE] ${text} >> ${filename}`)
        cb(code,filepath);
    });
};