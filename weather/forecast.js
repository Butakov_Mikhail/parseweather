const https = require('https'),
    parser = require('./parser');

module.exports = (url, cb) => {
    https.get(url, (res) => {
        let rawData = '';
        res.setEncoding('utf8');

        res.on('data', (chunk) => {
            rawData += chunk;
        }).on('end', () => {
            cb(null, parser(rawData));
        });
    }).on('error', (e) => {
        console.error(`Get error: ${e.message}`);
        cb(e.message, []);
    });
};