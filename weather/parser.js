const cheerio = require('cheerio'),
    prefix = '#twc-scrollabe tr.clickable',
    params = ['.hourly-time', '.description', '.temp', '.precip', '.humidity', '.wind'];
let selector = params.map(param => `${prefix} ${param}`).join();

module.exports = (text) => {
    let dom = cheerio.load(text);
    let data = dom(selector).map((i, e) => dom(e).text().trim()).get();
    let result = [];
    while (data.length > 0) {
        let time, weather, temp, rain, humid, wind;
        [time, weather, temp, rain, humid, wind, ...data] = data;
        result.push({time, weather, temp, rain, humid, wind});
    }

    return result;
};