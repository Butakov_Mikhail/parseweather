const http = require('http');
const fs = require('fs');
const { URL } = require('url')

let ask = require('./weather/forecast'),
    human = require('./weather/human'),
    voice = require('./voice'),
    urls = require('./urlconfig');
    
function handler(point,cb){
    ask(urls[point], (err, forecast) => {
        let humanstr = human(forecast[0]);
        voice(humanstr, cb)
    });
}

http.createServer((req, res) => {
    let point = new URL('http://localhost' + req.url).searchParams.get('point');
    console.log((new Date()).toJSON(), point);
    if (point && urls[point]) {
        handler(point, (err, filepath) => {
            var stat = fs.statSync(filepath);
            res.writeHead(200, {
                'Content-Type': 'audio/wav',
                'Content-Length': stat.size
            });
            fs.createReadStream(filepath).pipe(res);
            fs.unlink(filepath,()=>{});
        });
    } else {
        res.writeHead(400, { 'Content-Type': 'text/plain' });
        if (!point) {
            res.end('Bad request :(\nWe need parameter "point"');
        } else {
            res.end('Bad request :(\nWe don\'t know about "point"=' + point);
        }
    }
}).listen(process.env.PORT, process.env.IP);
